exports.install = function () {
    F.route('/', view_index)
    F.route('/about', view_about)
    F.route('/nhform', view_nhform)
    F.route('/', json_contact, ['put', 'post', 'json'])
    F.route('/all', view_QueryAll)
    // or
    // F.route('/')
}

function view_index() {
    var self = this
    self.view('index')
}

function view_about() {
    var self = this
    self.view('about')
}

function view_nhform() {
    var self = this
    self.view('nhform')
}

var json = require('jsonfile')
var myfile = '../files/template.txt'

var stuff = {
    self: 'this',
    model: 'self.body'
}

function json_contact() {
    var self = this
    var fs = require('fs')
    var m = require('./mail')

    var model = self.body
    // console.log(model)
    fs.readFile('./files/template.txt', 'utf8', (err, data) => {
        if (err) throw err
        for (i in model
        ) {
            data = data.replace(i, model[i])
        }
        // data = data.replace('empName', model.eName)
        // console.log(data)
        var filename = 'New Hire form for ' + model.eName + '.doc'

        fs.writeFile('./files/' + filename, data, 'utf8', (err) => {
            console.log('file created ')
            var message = Mail.create(filename, data)
            Mail.try('smtp.gmail.com', (err) => {
                if (err) throw err

                message.from('yikuno.barnaby@alexanderwang.com', 'Kino B')
                message.to('yikuno.barnaby@alexanderwang.com')
                message.attachment('./files/' + filename)
                message.send('smtp.exg6.exghost.com', { secure: "true", port: "587", user: '', password: '' })
                console.log('email sent')
            })
            this.redirect('/nhform')
        })
    })
}

function err(err) {
    if (err) throw err
}

function view_QueryAll() {
    var self = this
    var model = self.body
    json.readFile(myfile, err, obj => {
        err(err)
        render(obj)
    })
    self.view('seeall', model)
}