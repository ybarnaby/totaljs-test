let ldap = require('ldapjs')
function connectClient() {
  var client = ldap.createClient({
    url: 'ldap://' // insert ldap information here
  })
}

entry = {
  cn: 'jonhs',
  sn: 'longs',
  OU: 'To be O365 migrated',
  OU: 'NYC Office',
  DC: 'alexanderwang',
  DC: 'local',
  email: ['long.jonhs@alex.com']

}
function bind() {
  client.bind('sAMAccountName=', '', err => {
    if (err) throw err
    client.add('CN=johns, OU=2nd Floor', entry, err => {
      if (err) throw err
      console.log('completed')
    })
  })
}

// console.log(client)
